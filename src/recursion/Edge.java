package recursion;

public class Edge {
    int sourceVertex;
    int destinationVertex;

    public Edge(int sourceVertex, int destinationVertex){
        this.sourceVertex = sourceVertex;
        this.destinationVertex = destinationVertex;
    }
}
