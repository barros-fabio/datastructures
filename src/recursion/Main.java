package recursion;

import java.util.*;

public class Main {

    public static void main(String[] args)
    {
        // Creates the graph edges
        List<Edge> edges = Arrays.asList(
                new Edge(0, 1), new Edge(0, 4) ,new Edge(1, 2),
                new Edge(1, 4), new Edge(2, 3), new Edge(3, 4),
                new Edge(3, 5)
        );

        // Set the total number of vertices of the graph
        final int verticesNumber = 6;

        // creates and initializes a graph from the edges created above
        Graph graph = new Graph(edges, verticesNumber);

        // stores which vertices were already visited or not
        boolean[] isVisited = new boolean[verticesNumber];

        Stack stack = new Stack();

        // Starts searching from Vertex 0
        DepthFirstSearch.depthFirstSearchRecursive(graph,0 , isVisited, stack);
    }
}