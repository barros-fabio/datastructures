package recursion;

import java.util.ArrayList;
import java.util.List;

public class Graph {

    // Storing the adjacent Vertices for each given vertex
    List<List<Integer>> adjacentVertices = null;

    public Graph(List<Edge> edges, int verticesNumber){
        createGraph(edges,verticesNumber);
    }

    public void createGraph(List<Edge> edges, int verticesNumber){
        adjacentVertices = new ArrayList<>(verticesNumber);

        for (int i = 0; i < verticesNumber; i++) {
            adjacentVertices.add(i, new ArrayList<>());
        }

        for (int i = 0; i < edges.size(); i++)
        {
            int sourceVertex = edges.get(i).sourceVertex;
            int destinationVertex = edges.get(i).destinationVertex;

            adjacentVertices.get(sourceVertex).add(destinationVertex);
            adjacentVertices.get(destinationVertex).add(sourceVertex);
        }
    }
}

