package recursion;

import java.util.Stack;

public class DepthFirstSearch{

    public static void depthFirstSearchRecursive(Graph graph, int current, boolean[] isVisited, Stack stack)
    {
        // Marking the current vertex as visited
        isVisited[current] = true;

        // Print information about which vertex is being visited
        System.out.println("Vertex being visited: "+current + " ");
        System.out.println("Adjacent Vertices: ");
        System.out.println(graph.adjacentVertices.get(current));
        System.out.println("\n Vertices that were already visited: ");
        printVisitedVertices(isVisited);

        // Stack to store which calls to DFS are "on hold"
        stack.push(current);
        System.out.println("\n Stack of Vertices where DFS is being called: ");
        stack.stream().forEach(System.out::println);
        System.out.println(" ");
        System.out.println(" -------------------------------------------------- ");

        // for every node, go over its adjacent vertices to verify if they have been visited
        for (int destination : graph.adjacentVertices.get(current))
        {
            // If the adjacent destination vertex has never been visited, visit
            if (!isVisited[destination]) {
                depthFirstSearchRecursive(graph, destination, isVisited, stack);
            }
        }
        stack.pop();
    }

    public static void printVisitedVertices(boolean[] isVisited){
        for(int i = 0; i < isVisited.length; i++)
            System.out.println("Vertex: "+i+" -  Visited? "+isVisited[i]+" ");
    }

}
